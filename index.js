const express = require('express');
const app = express();
const route = express.Router();
const config = require('./config');
const uuid = require('uuid');
const http = require('http');
const path = require('path');
const fs = require('fs');

app.use((req, res, next) => {
    console.log(req.url);
    fs.appendFile('log.txt', req.url + '\n', (err) => {
        if (err) {
            console.log(err);
        }
    })
    next();
})
route.get('/', (req, res) => {

    res.send("Welcome");

})
route.get('/html', (req, res) => {
    res.status(200);
    console.log(req.url);
    res.sendFile(path.join(__dirname, "file1.html"));

})
route.get('/json', (req, res) => {

    res.status(200);
    res.sendFile(path.join(__dirname, "file2.json"));
})

route.get('/uuid', (req, res) => {

    res.status(200);
    res.set('Content-Type', 'application/json');

    const uuidNumber = uuid.v4()
    res.json({ "uuid": uuidNumber });

})
route.get('/status/:id', (req, res) => {

    const resData = req.params.id;


    if (http.STATUS_CODES[resData] && resData != 100) {

        res.status(Number(resData));
        res.set('Content-Type', 'application/json');
        res.json({ "status": resData })
    }
    else {
        res.status(400);
        res.set('Content-Type', 'application/json');
        res.json({ "status": "Invalid input" });

    }


})
route.get('/delay/:id', (req, res) => {

    const delay = req.params.id;


    if (delay >= 0) {
        setTimeout(() => {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "status 200 dealy by ": delay })
        }, delay * 1000);
    }
    else {
        res.status(404);
        res.set('Content-Type', 'application/json');
        res.json({ "status": "Invalid input" });

    }


})
route.get('/logs', (req, res) => {
    fs.readFile('log.txt', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send(data);
        }
    })
})

route.use((req, res) => {
    res.status(400);
    res.send("invalid input");
})

app.use('/', route);



app.listen(config.port, () => {
    console.log("server is running");
})